package com.myzee.messaging;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.fasterxml.jackson.databind.util.JSONPObject;

import jdk.nashorn.internal.parser.JSONParser;


@RestController
@RequestMapping(value = "/message")
public class MessagingDeliver {
	
	@Autowired
	RestTemplate restTemplate;
	
	@PostMapping(value = "/send/{message}")
	public String messageDelivery(@PathVariable("message") String msg) {
		
		String url = "http://MESSAGE-SERVICE/msg/send";
		Date d = new Date();
//		String inputMsgJson = "{\"message\":\"Hi, How r u?\"}";
		
		String key = "{\"message\":";
		String value = "\"" + msg + "\"}";
		
		
		String inputMsgJson = key + value;
//		String inputMsgJson = msg.getMessage().replaceAll('"', "\\""");
		System.out.println(inputMsgJson);
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		HttpEntity<String> entity = new HttpEntity<String>(inputMsgJson, headers);
		
		System.out.println("ready to send");
		String result = restTemplate.postForObject(url, entity, String.class);
		System.out.println("returning result " + result + "==");
		return result;
	}
	
	@GetMapping(value = "/list")
	public MessageList getMessages(){
		String url = "http://MESSAGE-SERVICE/msg/getlist";
		return restTemplate.getForObject(url, MessageList.class);
	}
}
