package com.myzee.messaging;

import java.util.List;


public class MessageList {
	List<Message> msgList;
	
	public MessageList() {
		// TODO Auto-generated constructor stub
	}

	public MessageList(List<Message> msgList) {
		super();
		this.msgList = msgList;
	}

	public List<Message> getMsgList() {
		return msgList;
	}

	public void setMsgList(List<Message> msgList) {
		this.msgList = msgList;
	}
	
	
}
